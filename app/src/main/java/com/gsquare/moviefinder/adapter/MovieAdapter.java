package com.gsquare.moviefinder.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.gsquare.moviefinder.R;
import com.gsquare.moviefinder.activities.MovieInformationActivity;
import com.gsquare.moviefinder.model.Movie;
import java.util.List;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder>{

    private Context mContext;
    private List<Movie> mResultList;

    public MovieAdapter(Context context, List<Movie> resultList) {
        mContext = context;
        mResultList = resultList;
    }

    @Override
    public MovieAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {

        View itemLayoutView =  LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_item_movie, viewGroup, false);

        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(final MovieAdapter.ViewHolder viewHolder, final int i) {

        viewHolder.mName.setText(mResultList.get(i).getmName());
        viewHolder.mType.setText(mResultList.get(i).getmMovieType());

        // Load the cover pic of outlet
        Glide.with(mContext)
                .load(mResultList.get(i).getmPosterLink())
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .into(viewHolder.mCoverImage);

        viewHolder.mResultCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callToDetails(mResultList.get(i));
            }
        });
    }

    public void callToDetails(Movie movie) {
        Intent i = new Intent(mContext, MovieInformationActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("MovieObject", movie);
        mContext.startActivity(i);
    }

    @Override
    public int getItemCount() {
        return mResultList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @InjectView(R.id.movie_image) ImageView mCoverImage;
        @InjectView(R.id.movie_name) TextView mName;
        @InjectView(R.id.movie_type) TextView mType;
        @InjectView(R.id.result_card) CardView mResultCard;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}