package com.gsquare.moviefinder.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.gsquare.moviefinder.R;
import com.gsquare.moviefinder.model.Rating;
import java.util.List;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ganesh on 04/04/17.
 */

public class RatingAdapter extends RecyclerView.Adapter<RatingAdapter.ViewHolder>{

    private Context mContext;
    private List<Rating> mResultList;

    public RatingAdapter(Context context, List<Rating> resultList) {
        mContext = context;
        mResultList = resultList;
    }

    @Override
    public RatingAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {

        View itemLayoutView =  LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_item_rating, viewGroup, false);

        return new RatingAdapter.ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(final RatingAdapter.ViewHolder viewHolder, final int i) {

        viewHolder.mSource.setText(mResultList.get(i).getmSource());
        viewHolder.mValue.setText(mResultList.get(i).getmValue());
    }

    @Override
    public int getItemCount() {
        return mResultList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @InjectView(R.id.txt_source) TextView mSource;
        @InjectView(R.id.txt_value) TextView mValue;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}