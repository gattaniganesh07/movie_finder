package com.gsquare.moviefinder.utils;

import android.content.Context;
import android.content.Intent;


public final class Util {

    private Util() {
		throw new AssertionError(getClass() + "Cannot Instanitate this class");
	}

    public static void shareText(Context mContext,String shareBody) {

        shareBody = shareBody + "\n via Movie Finder app";

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Movie Finder Android Application");
        share.putExtra(Intent.EXTRA_TEXT, shareBody);

        mContext.startActivity(Intent.createChooser(share, "Share link!"));
    }
}
