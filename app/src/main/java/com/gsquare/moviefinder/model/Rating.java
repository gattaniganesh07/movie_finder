package com.gsquare.moviefinder.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ganesh on 03/04/17.
 */

public class Rating implements Serializable {

    @SerializedName("Source")
    @Expose
    private String mSource;

    @SerializedName("Value")
    @Expose
    private String mValue;

    public String getmSource() {
        return mSource;
    }

    public void setmSource(String mSource) {
        this.mSource = mSource;
    }

    public String getmValue() {
        return mValue;
    }

    public void setmValue(String mValue) {
        this.mValue = mValue;
    }
}
