package com.gsquare.moviefinder.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

/**
 * Created by ganesh on 03/04/17.
 */

public class Movie implements Serializable {

    @SerializedName("Title")
    @Expose
    private String mName;

    @SerializedName("Released")
    @Expose
    private String mReleaseDate;

    @SerializedName("Runtime")
    @Expose
    private String mDuration;

    @SerializedName("Genre")
    @Expose
    private String mMovieType;

    @SerializedName("Director")
    @Expose
    private String mDirector;

    @SerializedName("Writer")
    @Expose
    private String mWriter;

    @SerializedName("Actors")
    @Expose
    private String mActor;

    @SerializedName("Plot")
    @Expose
    private String mPlot;

    @SerializedName("Poster")
    @Expose
    private String mPosterLink;

    @SerializedName("Ratings")
    @Expose
    private List<Rating> mRatingList;

    @SerializedName("Response")
    @Expose
    private String mResponse;

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmReleaseDate() {
        return mReleaseDate;
    }

    public void setmReleaseDate(String mReleaseDate) {
        this.mReleaseDate = mReleaseDate;
    }

    public String getmDuration() {
        return mDuration;
    }

    public void setmDuration(String mDuration) {
        this.mDuration = mDuration;
    }

    public String getmMovieType() {
        return mMovieType;
    }

    public void setmMovieType(String mMovieType) {
        this.mMovieType = mMovieType;
    }

    public String getmDirector() {
        return mDirector;
    }

    public void setmDirector(String mDirector) {
        this.mDirector = mDirector;
    }

    public String getmWriter() {
        return mWriter;
    }

    public void setmWriter(String mWriter) {
        this.mWriter = mWriter;
    }

    public String getmActor() {
        return mActor;
    }

    public void setmActor(String mActor) {
        this.mActor = mActor;
    }

    public String getmPlot() {
        return mPlot;
    }

    public void setmPlot(String mPlot) {
        this.mPlot = mPlot;
    }

    public String getmPosterLink() {
        return mPosterLink;
    }

    public void setmPosterLink(String mPosterLink) {
        this.mPosterLink = mPosterLink;
    }

    public List<Rating> getmRatingList() {
        return mRatingList;
    }

    public void setmRatingList(List<Rating> mRatingList) {
        this.mRatingList = mRatingList;
    }

    public String getmResponse() {
        return mResponse;
    }

    public void setmResponse(String mResponse) {
        this.mResponse = mResponse;
    }
}
