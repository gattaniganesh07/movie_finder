package com.gsquare.moviefinder.api;

import com.google.gson.JsonElement;
import com.gsquare.moviefinder.model.Movie;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SubmitResource
{

    @GET("/")
    Call<Movie> getMovieList(@Query("t") String t);

}

