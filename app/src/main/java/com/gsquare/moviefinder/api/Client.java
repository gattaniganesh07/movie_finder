package com.gsquare.moviefinder.api;

import android.content.Context;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Client {

    private static volatile OkHttpClient mClient;

    private Client() {
        mClient = new OkHttpClient();
    }

    public static OkHttpClient getClientInstance(Context context) {
        if (mClient == null) {
            synchronized (Client.class) {
                if (mClient == null) {
                    mClient = new OkHttpClient();
                    addResponseInterceptor(context);
                    setCache(context.getApplicationContext());
                }
            }
        }
        return mClient;
    }

    private static void setCache(Context context) {
        //TODO change cacheSize according to requirement
        final int cacheSize = 10 * 1024 * 1024; //10MB
        mClient.setCache(new Cache(context.getApplicationContext().getCacheDir(), cacheSize));
    }

    private static void addResponseInterceptor(final Context context) {
        mClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request r = chain.request();
                Response rp = chain.proceed(r);

                return rp;
            }
        });
    }


    public static final String BASE_URL = "http://www.omdbapi.com";
    private static Retrofit retrofit = null;

    private static SubmitResource gitApiInterface;
    public static SubmitResource getResult() {
        if (gitApiInterface == null) {

            OkHttpClient okClient = new OkHttpClient();
            okClient.setConnectTimeout(8, TimeUnit.SECONDS);
            okClient.setReadTimeout(8, TimeUnit.SECONDS);
            okClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response response = chain.proceed(chain.request());
                    return response;
                }
            });

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            gitApiInterface = client.create(SubmitResource.class);
        }
        return gitApiInterface;
    }

}