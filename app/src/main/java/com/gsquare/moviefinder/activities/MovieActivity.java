package com.gsquare.moviefinder.activities;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.gsquare.moviefinder.R;
import com.gsquare.moviefinder.adapter.MovieAdapter;
import com.gsquare.moviefinder.api.Client;
import com.gsquare.moviefinder.api.SubmitResource;
import com.gsquare.moviefinder.model.Movie;
import java.util.ArrayList;
import java.util.List;
import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieActivity extends AppCompatActivity {

    Context mContext;
    @InjectView(R.id.toolbar) Toolbar mToolbar;
    @InjectView(R.id.inputSearch) EditText mInputSearch;
    @InjectView(R.id.progress_bar) ProgressBar mMaterialProgressBar;
    @InjectView(R.id.my_recycler_view) RecyclerView mRecyclerView;
    @InjectView(R.id.no_data_layout) LinearLayout mNoDataLayout;
    @InjectView(R.id.txt_title) TextView mTitle;
    @InjectView(R.id.txt_subtitle) TextView mSubTitle;

    MovieAdapter movieAdapter;
    List<Movie> movieArrayList = new ArrayList<Movie>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mContext = MovieActivity.this;
        ButterKnife.inject(this);
        initialiseToolbar(getResources().getString(R.string.title_activity_movie));

        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
        movieAdapter = new MovieAdapter(mContext, movieArrayList);
        mRecyclerView.setAdapter(movieAdapter);

        // Search the text
        mInputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                fetchResult(cs.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    // Initialise toolbar title
    public void initialiseToolbar(String toolbarTitle) {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(toolbarTitle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    // hide the progressDialog
    public void hideProgressDialog() {
        mMaterialProgressBar.setVisibility(View.INVISIBLE);
    }

    // show the progressDialog
    public void showProgressDialog() {
        mMaterialProgressBar.setVisibility(View.VISIBLE);
    }

    public void fetchResult(String searchKey) {
        // Call to api through retrofit
        showProgressDialog();
        movieArrayList.clear();
        SubmitResource service = Client.getResult();

        Call<Movie> call = service.getMovieList(searchKey);
        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                if (response.body().getmResponse().equals("True")) {
                    movieArrayList.clear();
                    mNoDataLayout.setVisibility(View.GONE);
                    movieArrayList.add(response.body());
                    movieAdapter.notifyDataSetChanged();
                } else {
                    mNoDataLayout.setVisibility(View.VISIBLE);
                    mTitle.setText(getResources().getString(R.string.no_data_found));
                    mSubTitle.setText(getResources().getString(R.string.no_data_found_subtitle));
                }
                hideProgressDialog();
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                hideProgressDialog();
                mNoDataLayout.setVisibility(View.VISIBLE);
                mTitle.setVisibility(View.GONE);
                mSubTitle.setText(getResources().getString(R.string.retrofit_error_msg));
            }

        });
    }
}
