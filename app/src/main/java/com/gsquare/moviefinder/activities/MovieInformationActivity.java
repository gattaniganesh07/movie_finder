package com.gsquare.moviefinder.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.gsquare.moviefinder.R;
import com.gsquare.moviefinder.adapter.RatingAdapter;
import com.gsquare.moviefinder.model.Movie;
import com.gsquare.moviefinder.model.Rating;
import com.gsquare.moviefinder.utils.Util;
import java.util.ArrayList;
import java.util.List;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MovieInformationActivity extends AppCompatActivity {

    Context mContext;
    Movie movieObj;
    @InjectView(R.id.toolbar) Toolbar mToolbar;
    @InjectView(R.id.movie_image) ImageView mMovieImage;
    @InjectView(R.id.txt_director) TextView mDirector;
    @InjectView(R.id.txt_producer) TextView mProducer;
    @InjectView(R.id.txt_star_cast) TextView mStarCast;
    @InjectView(R.id.txt_movie_desc) TextView mMovieDesc;
    @InjectView(R.id.txt_movie_release_date) TextView mMovieDate;
    @InjectView(R.id.txt_duration) TextView mDuration;
    @InjectView(R.id.txt_share) TextView mShare;
    @InjectView(R.id.rating_recycler_view) RecyclerView mRecyclerView;
    @InjectView(R.id.txt_title_rating) TextView mTitle;

    RatingAdapter ratingAdapter;
    List<Rating> ratingArrayList = new ArrayList<Rating>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_information);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mContext = MovieInformationActivity.this;
        ButterKnife.inject(this);

        // get passed intent
        Intent intent = getIntent();
        movieObj = (Movie) intent.getSerializableExtra("MovieObject");

        initialiseToolbar(movieObj.getmName()+" movie");
        mDirector.setText("Director : " + movieObj.getmDirector());
        mProducer.setText("Writer : " + movieObj.getmWriter());
        mStarCast.setText("Stars : "  + movieObj.getmActor());
        mMovieDesc.setText("Plot : " +movieObj.getmPlot());
        mDuration.setText("Duration : " + movieObj.getmDuration());
        mMovieDate.setText("Release Date : "+movieObj.getmReleaseDate());

        if(movieObj.getmRatingList().size()>0) {
            mTitle.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.VISIBLE);
            ratingArrayList = movieObj.getmRatingList();
        } else {
            mTitle.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
        }

        // Load the cover pic of outlet
        Glide.with(mContext)
                .load(movieObj.getmPosterLink())
                .placeholder(R.mipmap.ic_launcher)
                .centerCrop()
                .into(mMovieImage);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
        ratingAdapter = new RatingAdapter(mContext, ratingArrayList);
        mRecyclerView.setAdapter(ratingAdapter);

    }

    public void initialiseToolbar(String toolbarTitle){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(toolbarTitle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.txt_share)
    public void share() {
        String shareBody = movieObj.getmName();
        Util.shareText(mContext,shareBody);
    }

    @OnClick(R.id.movie_image)
    public void movieImage() {
        String url = movieObj.getmPosterLink();
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        bundle.putString("title", ""+movieObj.getmName());

        Intent intent = new Intent(mContext, ImageZoomActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(bundle);
        mContext.startActivity(intent);
    }

}
