package com.gsquare.moviefinder.activities;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.gsquare.moviefinder.R;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class ImageZoomActivity extends AppCompatActivity {

    Context mContext;
    @InjectView(R.id.toolbar) Toolbar mToolbar;
    @InjectView(R.id.imageURL) ImageView mImage;
    String url, toolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_zoom);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mContext = ImageZoomActivity.this;
        ButterKnife.inject(this);
        url = super.getIntent().getExtras().getString("url");
        toolbarTitle = super.getIntent().getExtras().getString("title");
        initialiseToolbar(toolbarTitle);

        // Load the cover pic of outlet
        Glide.with(mContext)
                .load(url)
                .placeholder(R.mipmap.ic_launcher)
                .into(mImage);

    }

    public void initialiseToolbar(String toolbarTitle) {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(toolbarTitle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
